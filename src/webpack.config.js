var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var stylusLoader = ExtractTextPlugin.extract("style-loader", "css-loader!stylus-loader");

module.exports = {
    entry: "./app.js",
    output: {
        path: "./dest",
        filename: "bundle.css",
    },
    module: {
        loaders: [{
            test: /\.styl$/,
            loader: stylusLoader
        }]
    },
    plugins: [
        new ExtractTextPlugin("bundle.css")
    ]
};
